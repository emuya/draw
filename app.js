 require(["esri/views/MapView", "esri/Map", "esri/widgets/Sketch/SketchViewModel", "esri/geometry/geometryEngine", "esri/Graphic", "esri/layers/GraphicsLayer", "esri/layers/FeatureLayer",
 	"esri/widgets/Expand", "esri/widgets/FeatureForm", "esri/Color", "esri/request", "esri/layers/support/Field", "fcl/FlareClusterLayer_v4", "esri/symbols/SimpleMarkerSymbol",
 	"esri/symbols/SimpleLineSymbol", "esri/symbols/SimpleFillSymbol", "esri/symbols/TextSymbol", "esri/symbols/TextSymbol3DLayer", "esri/symbols/Font", "esri/renderers/ClassBreaksRenderer",
 	"esri/geometry/SpatialReference", "esri/symbols/PictureMarkerSymbol", "esri/widgets/Attribution"
 ], function(MapView, Map, SketchViewModel, geometryEngine, Graphic, GraphicsLayer, FeatureLayer, Expand, FeatureForm, Color, request, Field, fcl, SimpleMarkerSymbol, SimpleLineSymbol,
 	SimpleFillSymbol, TextSymbol, TextSymbol3DLayer, Font, ClassBreaksRenderer, SpatialReference, PictureMarkerSymbol, PictureMarkerSymbol, Attribution,) {
		
		
 	let distributorId, addGraphicGeom, editGraphic, highlight, featureForm, editArea, attributeEditing, updateInstructionDiv, editOn, sketchViewModel;
 	const token =
 		"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiOGNiNTA2YTYtYTdhZC00MDczLTg3OGUtMWVjNDQ5NTJiMThjIiwidXNlcm5hbWUiOiIrMjU0NzI1MzQ3ODIwIiwiZXhwIjoxNTc3ODkxNTk5LCJlbWFpbCI6ImJvcmxhbGVAZXNyaWVhLmNvbSIsInBob25lX251bWJlciI6IisyNTQ3MjUzNDc4MjAiLCJvcmlnX2lhdCI6MTU3NTI5OTU5OX0.CB9utJktjdTySa1y_w9Y5l7aUNW6Vvbr_xeW0S6XzmI"
 	const businessApi = "https://cors-anywhere.herokuapp.com/https://dev.api.marketforce360.com/api/v1/business/";
 	var maxSingleFlareCount = 10;
 	const template = {
 		title: "Zone: {name}",
 		fieldInfos: [{
 			fieldName: "name "
 		}]
 	};
 	let editFeature = {
 		"attributes": {
 			"name": "",
 			"businessid": ""
 		}
 	};
	
	
	// Zone Labels
        const labelClass = {
           symbol: {
            type: "text", 
            color: "green",
            haloColor: "black",
            font: {
              
                size: 10,
              weight: "bold"
            }
          },
          labelPlacement: "above-center",
          labelExpressionInfo: {
            expression: "$feature.name"
          }
        };
		
	// Zones Feature Layer
	
 	const featureLayer = new FeatureLayer({
 		url: "https://route.esriea.com/portal/sharing/servers/226df6f09dd34eb9b2c76ada9f01cd95/rest/services/zones/zones/FeatureServer/0",
 		outFields: ["*"],
 		popupTemplate: template,
		labelingInfo: [labelClass]
 	});
	
 	// GraphicsLayer to hold graphics created via sketch view model for the edit
 	const graphicsLayer = new GraphicsLayer({
 		id: "tempGraphics"
 	});
 	const map = new Map({
 		basemap: "streets-navigation-vector",
 		layers: [graphicsLayer, featureLayer]
 	});
 	var view = new MapView({
 		container: "viewDiv",
 		map: map,
 		center: [36.7, -1.2],
 		zoom: 10
 	});
 	var graphicCollection = [];
 	var fields = [{
 		name: "ObjectID",
 		alias: "ObjectID",
 		type: "oid"
 	}, {
 		name: "business_type_name",
 		alias: "Business Name",
 		type: "string"
 	}, {
 		name: "business_category",
 		alias: " Category",
 		type: "string"
 	}, ];
 	var fieldTemplate = {
 		title: "{name}",
 		content: "Something hapa..."
 	};
 	var defaultSym = new SimpleMarkerSymbol();
 	singlePoint = {
 		type: "picture-marker",
 		url: "./pins/business.png",
 		width: "32px",
 		height: "32px"
 	};
 	var renderer = new ClassBreaksRenderer({
 		defaultSymbol: defaultSym
 	});
 	renderer.field = "clusterCount";
 	let outline = {
 		color: "#fff",
 		width: 1
 	}
 	var smSymbol = new SimpleMarkerSymbol({
 		size:30,
 		outline: outline,
 		color: [0, 92, 230, 1]
 	});
 	var xlSymbol = new SimpleMarkerSymbol({
 		size: 40,
 		outline: outline,
 		color:[0, 92, 230, 1]
 	});
 	renderer.addClassBreakInfo(0, 9, smSymbol);
 	renderer.addClassBreakInfo(10, Infinity, xlSymbol);
 	var areaRenderer;
 	var defaultAreaSym = new SimpleFillSymbol({
 		style: "solid",
 		color: [0, 0, 0, 0.2],
 	});
 	view.when(function(e) {
 		console.log('map view loaded');
 		loadBusinesses();
 	}, function(err) {
 		console.error("failed to load MapView " + e);
 	});

 	function loadBusinesses() {
 		$.ajax({
 			url: businessApi,
 			type: 'GET',
 			dataType: 'json',
 			headers: {
 				'Authorization': 'Bearer ' + token
 			},
 			contentType: 'application/json; charset=utf-8',
 			success: function(data) {
 				var Points = [];
 				var i;
 				var jsonArrayLength = data['results'].length
 				for (i = 0; i < jsonArrayLength; i++) {
 					try {
 						var city = data['results'][i]['business_location']['city']
 						var business_category = data['results'][i]['business_category']
 						console.log(business_category)
 						var business_id = data['results'][i]['id']
 						var business_type_name = data['results'][i]['name']
 						var latitude = data['results'][i]['business_location']['latitude']
 						var longitude = data['results'][i]['business_location']['longitude']
 						if (business_category == "DISTRIBUTOR") {
 							distributorId = data['results'][i]['id']
 							featureLayer.definitionExpression = "businessid = '" + distributorId + "'"
 						}
 						if (business_category !== "DISTRIBUTOR") {
 							var point = {
 								type: "point",
 								x: longitude,
 								y: latitude
 							};
 							var businesses = new Object({
 								"ObjectID": i++,
 								"category": business_category,
 								"name": business_type_name,
 								"x": longitude,
 								"y": latitude
 							});
 							graphicCollection.push(businesses);
 						}
 					} catch (error) {
 						console.log("Some business points did not render")
 					}
 				}
 				createLayer(graphicCollection);

 				function createLayer(data) {
 					var options = {
 						clusterRenderer: renderer,
 						flareRenderer: renderer,
 						singleSymbol: singlePoint,
 						singlePopupTemplate: fieldTemplate,
 						spatialReference: new SpatialReference({
 							"wkid": 4326
 						}),
 						subTypeFlareProperty: "category",
 						singleFlareTooltipProperty: "name",
 						displaySubTypeFlares: true,
 						maxSingleFlareCount: maxSingleFlareCount,
 						clusterRatio: 75,
 						data: data
 					}
 					clusterLayer = new fcl.FlareClusterLayer(options);
 					map.add(clusterLayer);
 					clusterLayer.on("draw-complete", function() {
 						console.log('draw complete event callback');
 					});
 				}
 				// Edit FeatureLayer
 				view.when(function() {
 					editor = new Editor({
 						view: view,
 						container: document.createElement("div"),
 						layerInfos: [{
 							layer: featureLayer,
 							fieldConfig: [{
 								name: "name",
 								label: "Zone Name",
 							}, {
 								name: "businessid",
 								label: "Busiess ID",
 							}]
 						}]
 					});

 					function editThis() {
 						if (!editor.viewModel.activeWorkFlow) {
 							view.popup.visible = false;
 							editor.startUpdateWorkflowAtFeatureEdit(view.popup.selectedFeature);
 							view.popup.spinnerEnabled = false;
 						}
 					}
 				});
 			}
 		})
 	}
 	hideSaveButton()

 	function hideSaveButton() {
 		$('#saveBtn').attr('disabled' , true)
 		$("#zoneName").on('change keydown paste input', function() {
 			if ($("#zoneName").val() == "") {
 				$("#zoneName").addClass('highlight')
 			} else {
 			
 				$("#zoneName").removeClass('highlight')
				$('#saveBtn').attr('disabled' , false)
 			}
 		});
 	}
 	let sketchGeometry = null;
 	createSketchViewModel();

 	function createSketchViewModel() {
 		sketchViewModel = new SketchViewModel({
 			layer: graphicsLayer,
 			view: view,
 			updateOnGraphicClick: false,
 			defaultUpdateOptions: {
 				tool: 'reshape',
 			},
 			polygonSymbol: {
 				type: "simple-fill",
 				color: "rgba(255,255,255, 0.3)",
 				style: "solid",
 				outline: {
 					color: "black",
 					width: 1
 				}
 			}
 		});
 		//setUpClickHandler();
 		sketchViewModel.on(["create"], function(event) {
 			if (event.state == "complete") {
 				sketchGeometry = event.graphic.geometry;
 				console.log("Drawing Complete...");
 			}
 		});
 	}
 	var drawPolygonButton = document.getElementById("polygonButton");
	
 	hideSaveButton()
 	drawPolygonButton.onclick = function() {

 		editOn = false;
 		sketchViewModel.create("polygon");
 		setActiveButton(this);
 		console.log("Start creating polygons!");
 
 		document.getElementById("updateBtn").enabled = false;
 	}
 	// edit button
 	document.getElementById("updateBtn").onclick = function() {
		sketchViewModel.cancel();
  		editOn = true;
 		setActiveButton(this);
 		
		$('html').css('cursor','pointer');
 		document.getElementById("polygonButton").enabled = false;
 	};
 	// delete button
 	document.getElementById("deleteBtn").onclick = function() {
 		//Start editing process
 		let params = {
 			deleteFeatures: [editFeature]
 		};
 		featureLayer.applyEdits(params).then(function(editsResult) {
 			// Call selectFeature function to highlight the new feature.
 			if (editsResult.addFeatureResults.length > 0) {
 				const objectId = editsResult.addFeatureResults[0].objectId;
 			}
 			graphicsLayer.removeAll();
 			sketchViewModel.destroy();
 			createSketchViewModel();
 			setActiveButton();
 			document.getElementById("zoneName").value = "";
 			editOn = false;
 			document.getElementById("polygonButton").enabled = true;
 			document.getElementById("updateBtn").enabled = true;
			
 		}).catch(function(error) {
 			console.log("error = ", error);
 		});
 		featureLayer.definitionExpression = "OBJECTID > 1"
 	};
 	// reset button
 	document.getElementById("resetBtn").onclick = function() {
 		sketchViewModel.cancel();
 		graphicsLayer.removeAll();
 		setActiveButton();
 		editOn = false;
 		featureLayer.definitionExpression = "OBJECTID > 1";
 		document.getElementById("polygonButton").enabled = true;
 		document.getElementById("updateBtn").enabled = true;
 		document.getElementById("zoneName").value = "";
 		//document.getElementById("zoneType").value = "";
 	};
 	// save button
 	document.getElementById("saveBtn").onclick = function() {
 		hideSaveButton()
 		if (editOn == false) {
 			var polyGraphics;
 			const attributes = {};
 			attributes["name"] = document.getElementById("zoneName").value;
 			attributes["businessid"] = distributorId;
 			graphicsLayer.graphics.forEach(zone => {
 				polyGraphics = zone.geometry
 			});
 			const addFeature = new Graphic({
 				geometry: polyGraphics,
 				attributes: attributes
 			});
 			const promise = featureLayer.applyEdits({
 				addFeatures: [addFeature],
 				deleteFeatures: null
 			});
 		} else {
 			console.log(editFeature)
 			editFeature.attributes.name = document.getElementById("zoneName").value;
 			editFeature.attributes.businessid = distributorId;
 			let params = {
 				updateFeatures: [editFeature]
 			}
 			featureLayer.applyEdits(params).then(function(editsResult) {
 				if (editsResult.addFeatureResults.length > 0) {
 					const objectId = editsResult.addFeatureResults[0].objectId;
 					console.log("Object ID updated..." + objectId);
 				}
				
				
 			}).catch(function(error) {
 				console.log("===============================================");
 				console.error("[applyEdits] FAILURE: ", error.code, error.name, error.message);
 				console.log("error = ", error);
 			});
 			featureLayer.definitionExpression = "OBJECTID > 1"
 		}
 		graphicsLayer.removeAll();
 		sketchViewModel.destroy();
 		createSketchViewModel();
 		setActiveButton();
 		document.getElementById("zoneName").value = "";
 		//document.getElementById("zoneType").value = "";
 		editOn = false;
 		document.getElementById("polygonButton").enabled = true;
 		document.getElementById("updateBtn").enabled = true;
		
				
	
 	};
 	view.when(function() {
 		view.on("click", function(event) {
 			console.log("State: " + sketchViewModel.state);
 			if (sketchViewModel.state === "active") {
 				
 				console.log("State: " + sketchViewModel.state);
 				return;
 			}
 			view.hitTest(event).then(function(response) {
 				console.log(response);
 				if (editOn == true) {
 					var results = response.results;
 					if (results.length > 0) {
 						for (var i = 0; i < results.length; i++) {
 							editGraphic = results[i].graphic;
 							editFeature = editGraphic;
 							graphicsLayer.graphics.add(editGraphic);
 							sketchViewModel.update(editGraphic, {
 								tool: "reshape"
 							});
 							//2. Hide Feature from Feature Layer
 							featureLayer.definitionExpression = "OBJECTID <> " + editGraphic.attributes.objectid;
 							//3. Populate attributes on text boxes
 							document.getElementById("zoneName").value = editGraphic.attributes.name
 							break;
 						}
 					}
 				}
 			});
 		});
 		// Listen the sketchViewModel's update-complete and update-cancel events
 		sketchViewModel.on(["update", "undo", "redo"], function(event) {
 			console.log(event);
 			onGraphicUpdate(event);
 		});
 		// get the graphic as it is being updated
 	});
	
	
 	// Runs when sketchViewModel's update-complete or update-cancel
 	// events are fired.
 	function onGraphicUpdate(event) {
 		let graphic = '';
 		graphicsLayer.graphics.items.forEach(ele => {
 			graphic = ele;
 		});
 		let moveGraphicGeo = graphicsLayer.graphics.items[graphicsLayer.graphics.items.length - 1].geometry;
 		const toolType = event.toolEventInfo.type;
 		if (event.toolEventInfo && (toolType === "move-stop" || toolType === "reshape-stop")) {
 			console.log("reshape tool completed..");
 		}
 	}

 	function setActiveButton(selectedButton) {
 		view.focus();
 		var elements = document.getElementsByClassName("active");
 		for (var i = 0; i < elements.length; i++) {
 			elements[i].classList.remove("active");
 		}
 		if (selectedButton) {
 			selectedButton.classList.add("active");
 		}
 	}
 	view.ui.add("edit", {
 		position: "top-left",
 		index: 1
 	});
 	view.ui.add("update", {
 		position: "top-left",
 		index: 1
 	});
 	// Click Zoom
 	// Zoom
 	view.on("click", function(evt) {
 		var screenPoint = {
 			x: evt.x,
 			y: evt.y
 		};
 		view.hitTest(screenPoint).then(function(response) {
 			var clusters = response.results[0].graphic;
 			if (clusters != null) {
 				console.log(clusters.attributes.isCluster);
 				if (clusters.geometry.type == "point" && clusters.sourceLayer == null && clusters.attributes.isCluster == true) {
 					view.goTo({
 						center: [clusters.attributes.x, clusters.attributes.y],
 						zoom: view.zoom + 1,
 						duration: 2000
 					})
 				}
 			}
 		});
 	})
 	// On Hover
 	view.on("pointer-move", function(evt) {
 		var screenPoint = {
 			x: evt.x,
 			y: evt.y
 		};
 		view.hitTest(screenPoint).then(function(screenCoords) {
 			getGraphics(screenCoords);
 		});
 	});

 	function getGraphics(screenCoords) {
 		var graphics = screenCoords.results[0].graphic;
 		if (graphics != null) {
 			if (graphics.geometry.type == "point" && graphics.sourceLayer == null && graphics.attributes.ObjectID > 0) {
 				view.popup.open({
 					location: graphics.geometry,
 					features: [graphics]
 				});
 			} else {
 				view.popup.close();
 			}
 		}
 	}
 });